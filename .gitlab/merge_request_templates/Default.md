# Merge Request Template

**Related JIRA Ticket:** [Ticket Number and URL to ticket] 
**Title:** [Short, descriptive title of the merge request]  

## Description  
[Provide a clear and concise explanation of what this merge request does, why it’s needed, and any relevant context. Include details about the problem being solved or the feature being added.]

## Nature of the MR  
- [ ] Bug Fix  
- [ ] New Feature  
- [ ] Refactor  

## Testing  
**What was done:**  
[Describe the testing approach, such as unit tests, integration tests, manual testing, etc., and what scenarios were covered.]  

**How to run the tests:**  
[Provide step-by-step instructions for running the tests, e.g., commands like `npm test`, specific test files, or manual steps to verify the changes.]

## Review / Approval  
**Author:** [Your Name / Username]  
**Reviewer(s):** [Name(s) of the person/people who will review and approve the MR]  
**Status:** [Pending / Approved / Changes Requested]