<?php

namespace Vurbis\Punchout\Controller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Integration\Model\Oauth\Token;
use Magento\Framework\Exception\LocalizedException;

abstract class BaseController extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var Token
     */
    protected $tokenModel;

    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Token $tokenModel
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->tokenModel = $tokenModel;
    }

    protected function createJsonResponse(array $data, int $statusCode = 200)
    {
        $result = $this->resultJsonFactory->create();
        $result->setHttpResponseCode($statusCode);
        return $result->setData($data);
    }

    protected function isValidToken($token)
    {
        try {
            if (strpos($token, 'Bearer ') === 0) {
                $token = substr($token, 7);
            }
    
            $this->tokenModel->loadByToken($token);
            if ($this->tokenModel->getId() && $this->tokenModel->getType() == Token::TYPE_ACCESS) {
                return true;
            } else {
                throw new LocalizedException(__('Invalid access token.'));
            }
        } catch (LocalizedException $e) {
            throw $e;
        }
    }
}
