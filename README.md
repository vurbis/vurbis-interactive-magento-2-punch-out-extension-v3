
# Installation

## A) Manually Upload Module 

### Install and update module manually under app/code

If you want to upload the extension manually follow these steps:

1. Download the module:
	```
	https://gitlab.com/vurbis/vurbis-interactive-magento-2-punch-out-extension-v3
	```
2. Extract the package and upload to (If the path doesn't exist, create it):
	```
	 {YOUR-MAGENTO2-ROOT-DIR}/app/code/Vurbis/Punchout 
	```
3. After all files and folders are uploaded, run the following commands:
	```
	php bin/magento maintenance:enable
	php bin/magento setup:upgrade
	php bin/magento setup:di:compile
	php bin/magento setup:static-content:deploy
	php bin/magento maintenance:disable
	php bin/magento cache:flush
	```

### Remove module (If manually installed)

1. Remove all files at this location:
	```
	{YOUR-MAGENTO2-ROOT-DIR}/app/code/Vurbis/Punchout
	```
2. Modify config.php:
	```
	{YOUR-MAGENTO2-ROOT-DIR}/app/etc/config.php.
	```
	
	Remove the line with Vurbis_Punchout => 1

3. Run the following commands:
	```
	php bin/magento maintenance:enable
	php bin/magento setup:upgrade
	php bin/magento setup:di:compile
	php bin/magento setup:static-content:deploy
	php bin/magento maintenance:disable
	php bin/magento cache:flush
	```

## B) Composer Installation

### Install Module

Instead of manual upload, you can also install with Composer:

1. Run composer command (in the magento root path):
	```
	composer require vurbis/magento2-punchout-v3
	```
2. Run the following commands to install module:
	```
	php bin/magento maintenance:enable
	php bin/magento setup:upgrade
	php bin/magento setup:di:compile
	php bin/magento setup:static-content:deploy
	php bin/magento maintenance:disable
	php bin/magento cache:flush
	```

### Update plugin
1. Run the Composer update command:
	```
	composer update vurbis/magento2-punchout-v3
	```
2. Run the following commands to update the modules:
	```
	php bin/magento maintenance:enable
	php bin/magento setup:upgrade
	php bin/magento setup:di:compile
	php bin/magento setup:static-content:deploy
	php bin/magento maintenance:disable
	php bin/magento cache:flush
	```

### Uninstall Module
1. Run the Composer remove command:
	```
	composer remove vurbis/magento2-punchout-v3
	```
2. Delete the vendor directory for the module: 
	```
	rm -rf vendor/vurbis
	```
3. Run the following commands to update the modules:
	```
	php bin/magento maintenance:enable
	php bin/magento setup:upgrade
	php bin/magento setup:di:compile
	php bin/magento setup:static-content:deploy
	php bin/magento maintenance:disable
	php bin/magento cache:flush
	```
---

<div align="center">
  <a href="https://www.vurbis.com/"><img src="https://vurbis-portal.web.app/assets/vurbis/logos/VurbisBlue_logo.svg" width="200"></a>
<p>Copyright © 2019 Vurbis. All rights reserved. </p>
</div>
