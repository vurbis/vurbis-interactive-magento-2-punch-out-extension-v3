<?php
namespace Vurbis\Punchout\Cron;

use Psr\Log\LoggerInterface;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Registry;
use Magento\Framework\Exception\LocalizedException;
use Vurbis\Punchout\Model\Configuration;
use Magento\Framework\App\ResourceConnection;

/**
 * DeleteCustomers Cron
 * When the cron is enabled, it will delete customers with firstname "Punchout" and last lastname "OCI" that are older than the set amount of days.
 */
class DeleteCustomers
{
    protected $logger;
    protected $customerCollectionFactory;
    protected $date;
    protected $customerRepository;
    protected $registry;
    protected $configuration;
    protected $resourceConnection;
    const CONNECTION_NAME = 'vurbis';

    public function __construct(
        LoggerInterface $logger,
        CollectionFactory $customerCollectionFactory,
        Configuration $configuration,
        DateTime $date,
        CustomerRepositoryInterface $customerRepository,
        Registry $registry,
        ResourceConnection $resourceConnection
    ) {
        $this->logger = $logger;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->configuration = $configuration;
        $this->date = $date;
        $this->customerRepository = $customerRepository;
        $this->registry = $registry;
        $this->resourceConnection = $resourceConnection;
    }

    public function execute()
    {
        try {
            if (!$this->configuration->isCronEnabled()) {
                return;
            }
    
            $this->registry->register('isSecureArea', true);
    
            $days = (int)$this->configuration->getCronDays();
            $date = $this->date->gmtDate('Y-m-d H:i:s', strtotime("-{$days} days"));
    
            $customers = $this->customerCollectionFactory->create();
            $customers->addFieldToFilter('firstname', 'Punchout')
                      ->addFieldToFilter('lastname', 'OCI')
                      ->addFieldToFilter('created_at', ['lt' => $date]);
    
            $connection = $this->resourceConnection->getConnection(self::CONNECTION_NAME);
            $tableName = $connection->getTableName('customer_entity');

            $whereCondition = [
                'firstname = ?' => 'Punchout',
                'lastname = ?' => 'OCI',
                'created_at < ?' => $date
            ];

            $connection->delete($tableName, $whereCondition);
            
            $this->registry->unregister('isSecureArea');
        } catch(\Exception $e) {
            $this->logger->error('Vurbis DeleteCustomers cron error. ' . $e->getMessage());
            throw new LocalizedException(__('Error executing DeleteCustomers cron: ' . $e->getMessage()));
        }
    }

}
