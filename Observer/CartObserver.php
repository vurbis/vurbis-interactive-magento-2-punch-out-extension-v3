<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vurbis\Punchout\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CartObserver implements ObserverInterface
{
    /** 
     * @var \Psr\Log\LoggerInterface
     */
    private $_logger;
    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    private $_quoteRepository;
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $_session;
    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     * @param \Magento\Customer\Model\Session $session
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Customer\Model\Session $session    
    )
    {
        $this->_logger = $logger;
        $this->_quoteRepository = $quoteRepository;
        $this->_session = $session;
    }

    public function execute(Observer $observer)
    {   
        if($this->_session->getPunchoutIsOci() && 
                $this->_session->getPunchoutSession() && 
                $this->_session->getPunchoutCleanCustomerId()) {
            $cart = $observer->getData('cart');
            $quote = $this->_quoteRepository->get($cart->getQuote()->getId());
            $quote->setCustomerId(null)->save();
        }
    }
}